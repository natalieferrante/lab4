/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef ButtonDebounce_H
#define ButtonDebounce_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Debouncing, Ready2Sample
}ButtonState_t;

// Public Function Prototypes
bool InitButtonDebounce(uint8_t Priority);
bool PostButtonDebounceSM(ES_Event_t ThisEvent);
bool CheckButtonEvents(void);
ES_Event_t RunButtonDebounceSM(ES_Event_t ThisEvent);
ButtonState_t QueryButtonDebounceSM(void);

#endif /* FSMTemplate_H */

