/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef DecodeMorse_H
#define DecodeMorse_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
# include "ES_Events.h"

// Public Function Prototypes

bool InitMorseDecode(uint8_t Priority);
bool PostDecodeMorse(ES_Event_t ThisEvent);
bool CheckMorseDecodeEvents(void);
ES_Event_t RunMorseDecode(ES_Event_t ThisEvent);

#endif /* FSMTemplate_H */

