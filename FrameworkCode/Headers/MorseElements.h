/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef MorseElements_H
#define MorseElements_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
# include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitMorseElements, CalWaitForRise, CalWaitForFall,
  EOC_WaitRise, EOC_WaitFall, DecodeWaitRise, DecodeWaitFall
}State_t;

// Public Function Prototypes

bool InitializeMorseElements(uint8_t Priority);
bool PostMorseElements(ES_Event_t ThisEvent);
bool CheckMorseEvents(void);
ES_Event_t RunMorseElementsSM(ES_Event_t ThisEvent);
State_t QueryMorseElements(void);

#endif /* FSMTemplate_H */

