/****************************************************************************
 Module
   ButtonDebounce.c

 Revision
   1.0.1

 Description
   a service that implements a state machine, Button module

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TemplateFSM.h"
#include "ButtonDebounce.h"
#include "ShiftRegisterWrite.h"
#include "ES_Timers.h"
#include "MorseElements.h"
#include "DecodeMorse.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/

//ButtonState_t CurrentState;
uint8_t LastButtonState;


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static ButtonState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitButtonDebounce

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
    Initialize the MyPriority variable with the passed in parameter.
    Initialize the port line to monitor the button
    Sample the button port pin and use it to initialize LastButtonState
    Set CurrentState to be Debouncing
    Start debounce timer (timer posts to ButtonDebounceSM)
    End of InitializeButton (return True)
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitButtonDebounce(uint8_t Priority)
{
  
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  
  //  Initialize the port line to receive Morse code
  SR_Init();
	//set bits as digital, set data direction input
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DEN)) |= (BIT4HI); 
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DIR)) &= (BIT4LO);
  
  //Set BIT4 as pullup
  HWREG(GPIO_PORTB_BASE+GPIO_O_PUR) |= BIT4HI;
  
  LastButtonState = HWREG(GPIO_PORTB_BASE +(GPIO_O_DATA + ALL_BITS));
  
  // put us into the Initial PseudoState
  CurrentState = Debouncing;
  
  ES_Timer_InitTimer(1,50);
  
  // post the initial transition event
 // ThisEvent.EventType = ES_INIT;
  
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostButtonDebounceSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostButtonDebounceSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
   CheckButtonEvents
 Parameters
   None
 Returns
   bool: returns True if an event was posted
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool CheckButtonEvents(void)
{
  
//  Set CurrentButtonState to state read from port pin
//If the CurrentButtonState is different from the LastButtonState
//	Set ReturnVal = True
//	If the CurrentButtonState is down
//		PostEvent ButtonDown to ButtonDebounce queue
//	Else
//		PostEvent ButtonUp to ButtonDebounce queue
//	Endif
//Endif
//Set LastButtonState to the CurrentButtonState

  static uint16_t LastButtonState = 0;
  uint16_t CurrentButtonState;
  bool ReturnVal = false;

  CurrentButtonState = HWREG(GPIO_PORTB_BASE +(GPIO_O_DATA + ALL_BITS)) & BIT4HI;
  //printf("%u \n\r", CurrentButtonState);
  if (CurrentButtonState != LastButtonState) // event detected, so post detected event
  {
    ReturnVal = true;
    if (CurrentButtonState == 0) {
      printf("Button Down! \n\r");
      ES_Event_t ThisEvent;
      ThisEvent.EventType = DBButtonDown;
      PostButtonDebounceSM(ThisEvent); 
    }else{
      printf("Button Up! \n\r");
      ES_Event_t ThisEvent;
      ThisEvent.EventType = DBButtonUp;
      PostButtonDebounceSM(ThisEvent);
    }
  }
  
  LastButtonState = CurrentButtonState; // update the state for next time

  return ReturnVal;
}
/****************************************************************************
 Function
    RunButtonDebounceSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   implements a 2-state state machine for debouncing timing
 Notes
   The EventType field of ThisEvent will be one of: ButtonUp, ButtonDown, 
   or ES_TIMEOUT
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunButtonDebounceSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState){
    
    case Debouncing: {
      
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 1){
        CurrentState = Ready2Sample;
      }
    }
      break;
      
    case Ready2Sample:{
        if(ThisEvent.EventType == DBButtonUp){
          ES_Timer_InitTimer(1,50);
          CurrentState = Debouncing;
          ES_Event_t ThisEvent;
          ThisEvent.EventType = DBButtonUp;
          PostMorseElements(ThisEvent);
          PostDecodeMorse(ThisEvent);
        }
        if(ThisEvent.EventType == DBButtonDown){
          ES_Timer_InitTimer(1,50);
          CurrentState = Debouncing;
          ES_Event_t ThisEvent;
          ThisEvent.EventType = DBButtonDown;
          PostMorseElements(ThisEvent);
          PostDecodeMorse(ThisEvent);
        }
    }
     break;

    
    default:
      ;
  }
  
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
ButtonState_t QueryButtonDebounceSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

