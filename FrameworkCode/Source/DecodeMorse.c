/****************************************************************************
 Module
   DecodeMorse.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"

#include "DecodeMorse.h"
#include "LCDService.h"
#include <string.h>

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

char DecodeMorseString(void);


/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
//static DecodeState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

static char LegalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= !$&+;@_";
static char MorseCode[][8] ={ ".-","-...","-.-.","-..",".","..-.","--.","....","..",
  ".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-",
  "...-",".--","-..-","-.--","--..",".----","..---","...--","....-",".....",
  "-....","--...","---..","----.","-----","..--..",".-.-.-","--..--","---...",
  ".----.","-....-","-..-.","-.--.-","-.--.-",".-..-.","-...-","-.-.--","...-..-",
  ".-...",".-.-.","-.-.-.",".--.-.","..--.-"};
char MorseString[7] = "";
char CharString[7] = "";

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     implements the service for Morse Decode
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMorseDecode(uint8_t Priority)
{
//  Clear (empty) the MorseString variable
  MyPriority = Priority;
  MorseString[0] = '\0';
  
  return true;
  // post the initial transition event
  //ThisEvent.EventType = ES_INIT;
//  if (ES_PostToService(MyPriority, ThisEvent) == true)
//  {
//    return true;
//  }
//  else
//  {
//    return false;
//  }
}

/****************************************************************************
 Function
     PostMorseDecode

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDecodeMorse(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMorseDecode

 Parameters
   The EventType field of ThisEvent will be one of: DotDetectedEvent, 
   DashDetectedEvent, EOCDetected, EOWDetected, ButtonDown.

 Returns
   Returns ES_NO_EVENT if No Error detected, ES_ERROR otherwise

 Description
   add your description here
 Notes
   Returns ES_NO_EVENT if No Error detected, ES_ERROR otherwise
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMorseDecode(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  //printf("Running \n\r");

  switch (ThisEvent.EventType){
    case DotDetected:{
//      If there is room for another Morse element in the internal representation
//			   Add a Dot to the internal representation
//		  Else
//			   Set ReturnValue to ES_ERROR with param set to indicate this location
      //printf("DotDetected \n\r");
      if(strlen(MorseString) < 7){
        char dot = '.';
        strcat(MorseString, &dot);
      }else{
        ReturnEvent.EventType = ES_ERROR;
        ReturnEvent.EventParam = 1;
      }
    }
    break;
    
    
    case DashDetected:{
//		If there is room for another Morse element in the internal representation
//			Add a Dash to the internal representation
//		Else
//			Set ReturnValue to ES_ERROR with param set to indicate this location
      //printf("DashDetected \n\r");
      if(strlen(MorseString) < 7){
        char dash = '-';
        strcat(MorseString, &dash);
      }else{
        ReturnEvent.EventType = ES_ERROR;
        ReturnEvent.EventParam = 2;
      }
    }
    break;
      
      
    case EOCDetected:{
      //printf("EOCDetected\n\r");
//		call DecodeMorse to try and match current MorseString
//		Print to LCD the decoded character
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_LCD_PUTCHAR;
      ThisEvent.EventParam = DecodeMorseString();
      PostLCDService(ThisEvent);
//		Clear (empty) the MorseString variable
      MorseString[0] = 0;
    }
    break;      
   

    case EOWDetected:{
      //printf("EOWDetected\n\r");
//		call DecodeMorse to try and match current MorseString
//		Print to LCD the decoded character
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_LCD_PUTCHAR;
      ThisEvent.EventParam = DecodeMorseString();
      PostLCDService(ThisEvent);
//		Print to the LCD a space
      ES_Event_t ThisEvent2;
      ThisEvent2.EventType = ES_LCD_PUTCHAR;
      ThisEvent2.EventParam = ' ';
      PostLCDService(ThisEvent2);
//		Clear (empty) the MorseString variable
      MorseString[0] = 0;
    }
    break;
      
      
    case BadSpace:{
//		Clear (empty) the MorseString variable
      MorseString[0] = 0;
    }
    break;  
      
      
    case BadPulse:{
//		Clear (empty) the MorseString variable
      MorseString[0] = 0;
    }
    break;    


    case DBButtonDown:{
//		Clear (empty) the MorseString variable
      MorseString[0] = 0;
    }
    break;       
      
    default:
      ;
  }                                   
  return ReturnEvent;
}

/****************************************************************************
 Function
     DecodeMorseString

 Parameters
     None

 Returns
     Takes no parameters, returns either a character or a '~' indicating failure
 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
char DecodeMorseString(void){
  //printf("In Decode Morse String \n\r"); 
//		For every entry in the array MorseCode
//			If MorseString is the same as current position in MorseCode 
//				return contents of current position in LegalChars
//			EndIf
//		EndFor
//return '~', since we didn't find a matching string in MorseCode
  //printf("DecodeMorse\n\r");
  for (int i = 0; i <= 47; i++){
    if(!strcmp(MorseString, MorseCode[i])){
      //printf("Letter found\n\r");
      //printf(" %s", LegalChars[i]); 
      return LegalChars[i];
    }
  }
  return '~';
}



/***************************************************************************
 private functions
 ***************************************************************************/

