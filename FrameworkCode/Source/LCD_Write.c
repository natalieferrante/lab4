/***************************************************************************
 Module
   LCDWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to an LCD display in 4-bit
   mode with the actual data written to a shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/12/15 15:15 jec     first pass
 
****************************************************************************/
//----------------------------- Include Files -----------------------------*/

////Define test harness
//#define TEST
///*Test function */
//# include <stdio.h>


// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>

#include "BITDEFS.H"
#include "ShiftRegisterWrite.h"
#include "LCD_Write.h"

// Private functions
static void LCD_SetData4(uint8_t NewData);
static void LCD_PulseEnable(void);
static void LCD_RegisterSelect(uint8_t WhichReg);
static void LCD_Write4(uint8_t NewData);
static void LCD_Write8(uint8_t NewData);

// module level defines
#define LSB_MASK 0x0f
#define LCD_COMMAND 0
#define LCD_DATA    1
#define NUM_INIT_STEPS 10
#define USE_4_BIT_WRITE 0x8000

// these are the iniitalization values to be written to set up the LCD
static const uint16_t InitValues[NUM_INIT_STEPS] = {
    0x03, /* multi-step process to get it into 4-bit mode */
    0x03,  
    0x03,  
    0x02,  
    0x20, /* 4-bit data width, 1 line, 5x7 font */
    0x08, /* turn off the display */
    0x01, /* clear the display */
    0x07, /* increment and shift with each new character */
    0x0f, /* turn on display, cursor & blink */
    0x97 /* position cursor */};

// these are the delays between the initialization steps.
// the first delay is the power up delay so there is 1 more entry
// in this table than in the InitValues Table    
static const uint16_t InitDelays[NUM_INIT_STEPS+1] = {
    65535, /* use max delay for powerup */
     4100,
      100,
      100,
      100,
       53,
       53,
     3000,
       53,
       53,
       53 };

// place to keep track of which regiter we are writing to
  static uint8_t RegisterSelect=0;


/****************************************************************************
 Function
   LCD_HWInit
 Parameters
   None
 Returns
   Nothing
 Description
   Initializes the port hardware necessary to write to the LCD
 Notes
   This implementation uses the lower level shift register library so
   it simply calls the init for that library
 Author
   J. Edward Carryer, 10/12/15, 15:22
****************************************************************************/
void LCD_HWInit(void){
  // init the shift Register module
  SR_Init();
}

/****************************************************************************
 Function
   LCD_TakeInitStep
 Parameters
   None
 Returns
   uint16_t the number of uS to delay before the next step
 Description
   steps through the initialization value array with each call and send the
   initialization value to the LCD and returns the time to delay until the 
   next step. Return of 0 indicates the process is complete.
 Notes
   
 Author
   J. Edward Carryer, 10/12/15, 15:22
****************************************************************************/
uint16_t LCD_TakeInitStep(void){
  uint16_t CurrentInitValue;
  uint16_t Delay;
  
  // for keeping track of the initialization step
  // 0 is a special case of powerup, then it takes on 1 to NUM_INIT_STEPS       
  static uint8_t CurrentStep = 0;
  //printf("\nCurrentStep %d\r\n", CurrentStep);
  
  // if we have completed the initialization steps (check R/S pin is high (Q1))
  if(CurrentStep > NUM_INIT_STEPS){ 
    //set Delay value to zero
    Delay = 0;
  }else {
    
    // if we are just getting started, this is a special case 
    if(CurrentStep == 0){
      // and we inc the CurrrentStep and set the Delay to the first entry in the 
      //InitDelays table
      Delay = InitDelays[0];
      CurrentStep++;
        
    }else{
      // normal step, so grab the correct init value into CurrentInitValue
      CurrentInitValue = InitValues[CurrentStep-1];
      //printf("\nCurrent Initialization Value %d\r\n", CurrentInitValue); 
      // check to see if this is a 4-bit or 8-bit write and do the right kind
      if(CurrentStep < 4){
        LCD_WriteCommand4(CurrentInitValue);
      }else{
        LCD_WriteCommand8(CurrentInitValue);
      }
      
      // grab the correct delay for this step
      Delay = InitDelays[CurrentStep];
      // set up CurrentStep for next call 
      CurrentStep++;
    }
  }

  return Delay;
}

/****************************************************************************
 Function
   LCD_WriteCommand4
 Parameters
   uint8_t NewData; the 4 LSBs are written
 Returns
   Nothing
 Description
   clears the register select bit to select the command register then
   writes the 4 bits of data, then pulses (raises, then lowers) the enable 
   line on the LCD
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 16:18
****************************************************************************/
void LCD_WriteCommand4(uint8_t NewData){
  // clear the register select bit
  LCD_RegisterSelect(LCD_COMMAND);
  // write the 4 LSBs to the shift register
  LCD_Write4(NewData);
}

/****************************************************************************
 Function
   LCD_WriteCommand8
 Parameters
   uint8_t NewData; 
 Returns
   Nothing
 Description
   clears the register select bit to select the command register then
   writes all 8 bits of data, using 2 4-bit writes
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 16:26
****************************************************************************/
void LCD_WriteCommand8(uint8_t NewData){
  // clear the register select bit
  LCD_RegisterSelect(LCD_COMMAND);
  // write all 8 data bits to the shift register
  LCD_Write8(NewData);
}

/****************************************************************************
 Function
   LCD_WriteData8
 Parameters
   uint8_t NewData; 
 Returns
   Nothing
 Description
   sets the register select bit to select the data register then
   writes all 8 bits of data, using 2 4-bit writes
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 16:28
****************************************************************************/
void LCD_WriteData8(uint8_t NewData){
  // set the register select bit
  LCD_RegisterSelect(LCD_DATA);

  // write all 8 bits to the shift register in 2 4-bit writes
  LCD_Write8(NewData);
}


//********************************

// These functions are private to the module
//********************************
/****************************************************************************
 Function
   LCD_Write4
 Parameters
   uint8_t NewData; the 4 LSBs are written
 Returns
   Nothing
 Description
   writes the 4 bits of data, then pulses (raises, then lowers) the enable 
   line on the LCD
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 15:58
****************************************************************************/
static void LCD_Write4(uint8_t NewData){
  // put the 4 bits of data onto the LCD data lines
  LCD_SetData4(NewData); 
  
  // pulse the enable line to complete the write
  LCD_PulseEnable();
}

/****************************************************************************
 Function
   LCD_Write8
 Parameters
   uint8_t NewData; all 8 bits are written
 Returns
   Nothing
 Description
   writes the 8 bits of data, pulssing the Enable line between the 4 bit writes
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 15:58
****************************************************************************/
static void LCD_Write8(uint8_t NewData){
  uint8_t CurrentValue;
  
  // put the 4 MSBs of data onto the LCD data lines
  CurrentValue = NewData>>4;
  LCD_SetData4(CurrentValue);
  
  // pulse the enable line to complete the write
  LCD_PulseEnable();
  
  // put the 4 LSBs of data onto the LCD data lines
  CurrentValue = NewData;
  CurrentValue &= (BIT7LO & BIT6LO & BIT5LO & BIT4LO);
  LCD_SetData4(CurrentValue);
  
  // pulse the enable line to complete the write
  LCD_PulseEnable();
}

/****************************************************************************
 Function
   LCD_RegisterSelect
 Parameters
   uint8_t Which; Should be either LCD_COMMAND or LCD_DATA
 Returns
   Nothing
 Description
   sets the port bit that controls the register select (Q1) to the requested value
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the register select port bit
 Author
   J. Edward Carryer, 10/12/15, 15:28
****************************************************************************/
static void LCD_RegisterSelect(uint8_t WhichReg){
//  uint8_t CurrentValue;
  
    //value will be either LCD_COMMAND or LCD_DATA which is either 1 or 0
    RegisterSelect = WhichReg; 
}

/****************************************************************************
 Function
   LCD_SetData4
 Parameters
   uint8_t NewData; only the 4 LSBs are used
 Returns
   Nothing
 Description
   sets the 4 data bits to the LCD to the requested value and places the
   register select value in the correct Bit position (bit 1)
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the data pins
 Author
   J. Edward Carryer, 10/12/15, 15:42
****************************************************************************/
static void LCD_SetData4(uint8_t NewData){
  uint8_t CurrentValue;
  
  // get the current value of the port so that we can preserve the other
  // bit states 
  CurrentValue = 0x00;
  
  
  // insert the current state of RegisterSelect into bit 1
  if(RegisterSelect == 1){
    CurrentValue |= BIT1HI;
  }else{
    CurrentValue &= BIT1LO;
  }

  // put the 4 LSBs into the 4 MSB positions to apply the data to the
  // correct LCD inputs while preserving the states of the other bits
  NewData = NewData << 4;
  NewData = NewData | CurrentValue;
  
  // now write the new value to the shift register
  SR_Write(NewData);
  //printf("\n %d\n", NewData);
}

/****************************************************************************
 Function
   LCD_PulseEnable
 Parameters
   Nothing
 Returns
   Nothing
 Description
   pulses (raises, then lowers) the enable line on the LCD
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the data pin connected
   to the Enable pin on the LCD (bit 0 on the shift register)
 Author
   J. Edward Carryer, 10/12/15, 15:42
****************************************************************************/
static void LCD_PulseEnable(void){
  uint8_t CurrentValue;
  
  // get the current value of the port so that we can preserve the other
  // bit states
  CurrentValue = SR_GetCurrentRegister();
  
  // set the LSB of the byte to be written to the shift register
  CurrentValue |= BIT0HI;
  
  // now write the new value to the shift register
  SR_Write(CurrentValue);
  
  // clear the LSB of the byte to be written to the shift register
  CurrentValue &= BIT0LO;
  
  // now write the new value to the shift register
  SR_Write(CurrentValue);
}



/****************************************************************************/
////TEST HARNESS
//#ifdef TEST
///*Test Harness for testing this module*/
//#include "termio.h"

//#include <stdint.h>
//#include <stdbool.h>
//#include <stdio.h>

//#include "inc/hw_types.h"
//#include "inc/hw_memmap.h"
//#include "driverlib/sysctl.h"

//#include "ES_Configure.h"
//#include "ES_Framework.h"
//#include "ES_Port.h"
//#include "termio.h"
//#include "EnablePA25_PB23_PD7_PF0.h"

//int main(void){
//	TERMIO_Init();
//  
//	// When doing testing, it is useful to announce just which program
//	// is running.
//	puts("\rStarting Basic Template \r");
//	printf("%s %s\n",__TIME__, __DATE__);
//	printf("\n\r\n");
//  
//  puts("\nHello\n");
//  
//  //initialize the LCD hardware
//  LCD_HWInit();
//  
//  //Run the initialization (each step with a keystroke)
//  uint16_t delay = LCD_TakeInitStep();
//  printf("\nDelay %d\n", delay);
//  int counter = 15;
//  while(counter >= 0){
//    getchar();
//    uint16_t delay = LCD_TakeInitStep();
//    
//    printf("\nDelay %d\n", delay);
//    counter--;
//  }
//  
//  printf("\nstart\n");
//  //Write a character to the screen
//  while(true){
//    LCD_WriteData8(getChar());
//  }
//  
//  //sit at the end after all done
//  for(;;)
//	  ;
//}

//#endif
