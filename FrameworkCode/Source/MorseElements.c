/****************************************************************************

Morse Elements module (a service that implements a state machine)

Data private to the module: MyPriority, CurrentState, TimeOfLastRise, TimeOfLastFall, 
LengthOfDot, FirstDelta, LastInputState


 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/

////Define test harness
//#define TEST
///*Test function */
//# include <stdio.h>


/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MorseElements.h"
#include "ShiftregisterWrite.h"
#include "ButtonDebounce.h"
#include "DecodeMorse.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
void TestCalibration(void);
void CharacterizeSpace(void);
void CharacterizePulse(void);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static State_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

static uint8_t LastInputState;
static uint16_t TimeOfLastRise;
static uint16_t TimeOfLastFall;
static uint16_t LengthOfDot;
static uint16_t FirstDelta;
static uint16_t SecondDelta;



/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitializeMorseElements

 Parameters
    Takes a priority number, returns True. 

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitializeMorseElements(uint8_t Priority)
{
  

//  Set CurrentState to be InitMorseElements
//  Set FirstDelta to 0

  //  Initialize the MyPriority variable with the passed in parameter.
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  
  //  Initialize the port line to receive Morse code
  SR_Init();
	//set bits as digital, set data direction outputs
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DEN)) |= (BIT3HI); 
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DIR)) &= (BIT3LO);
  
  //  Sample port line and use it to initialize the LastInputState variable
  LastInputState = HWREG(GPIO_PORTB_BASE +(GPIO_O_DATA + ALL_BITS));
  
  // Set CurrentState to be InitMorseElements
  // put us into the Initial PseudoState
  CurrentState = InitMorseElements;
  
  // Set FirstDelta to 0
  FirstDelta = 0;
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  
//  Post Event ES_Init to MorseElements queue (this service)
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMorseElementsService

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMorseElements(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}


/****************************************************************************
 Function
   CheckMorseEvents
 Parameters
   None
 Returns
   bool: returns True if an event was posted
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool CheckMorseEvents(void)
{
  //static uint16_t  LastInputState = HWREG(GPIO_PORTB_BASE +(GPIO_O_DATA + ALL_BITS));
  uint16_t CurrentInputState;
  bool ReturnVal = false;

//  Get the CurrentInputState from the input line
  CurrentInputState = HWREG(GPIO_PORTB_BASE +(GPIO_O_DATA + ALL_BITS)) & BIT3HI;
//  
  
//If the state of the Morse input line has changed
  if (CurrentInputState != LastInputState) // event detected, so post detected event
  {
    
    if (CurrentInputState == BIT3HI) {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = RisingEdge;
      ThisEvent.EventParam  = ES_Timer_GetTime(); //check this is right timer
      ES_PostAll(ThisEvent);
      ReturnVal = true;
      //printf("R \n\r");
    }
    
    else{
      ES_Event_t ThisEvent;
      ThisEvent.EventType = FallingEdge;
      ThisEvent.EventParam = ES_Timer_GetTime(); //check this is right timer
      ES_PostAll(ThisEvent);
      ReturnVal = true;
      //printf("F \n\r");
    }
  }
  
  LastInputState = CurrentInputState; // update the state for next time

  
  return ReturnVal;
}



/****************************************************************************
 Function
    RunMorseElementsSM 
 Parameters
   The EventType field of ThisEvent will be one of: ES_Init, RisingEdge, 
  FallingEdge, CalibrationCompleted, EOCDetected, DBButtonDown.  
  The parameter field of the ThisEvent will be the time that the event occurred.


 Returns
   Returns ES_NO_EVENT 

 Local Variables: NextState

 Description
   Implements the state machine for Morse Elements

 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMorseElementsSM(ES_Event_t ThisEvent)
{  
  State_t NextState = CurrentState;
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState){
    
    case InitMorseElements:{
      //printf("In InitMorseElements \n\r");
//      If ThisEvent is ES_Init
//				Set NextState to CalWaitForRise
//			Endif 
      if (ThisEvent.EventType == ES_INIT){
        NextState = CalWaitForRise;
      }
    }
    break;
    
    
    case CalWaitForRise:{
     // printf("In CalWaitForRise \n\r");
//      If ThisEvent is RisingEdge
//				Set TimeOfLastRise to Time from event parameter
//				Set NextState to CalWaitForFall
//			Endif 
//			If ThisEvent is CalibrationComplete
//				Set NextState to EOC_WaitRise
//			Endif 
      if(ThisEvent.EventType ==  RisingEdge){ 
        TimeOfLastRise = ThisEvent.EventParam;
        NextState = CalWaitForFall; 
      }else if (ThisEvent.EventType ==  CalibrationCompleted){   
        NextState = EOC_WaitRise;
      }
    }
    break;
    
    
    case CalWaitForFall:{
      //printf("In CalWaitForFall \n\r");
      if (ThisEvent.EventType == FallingEdge){
//				Set TimeOfLastFall to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Call TestCalibration function
        printf("Running Test Calibration \n\r");
        TestCalibration();
//				EndIf
      }
    }
    break;
    
    
    case EOC_WaitRise:{
      //printf("In EOC_WaitRise \n\r");
      if (ThisEvent.EventType == RisingEdge){
//				Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
//				Set NextState to EOC_WaitFall
        NextState = EOC_WaitFall;
//				Call CharacterizeSpace function
        CharacterizeSpace();
        //printf("Stuck in EOC_WaitRise \n\r");
      }
      if (ThisEvent.EventType == DBButtonDown){
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
      //printf("Got through EOC_WaitRise \n\r");
    }
    break;
    
    
    case EOC_WaitFall:{
      //printf("In EOC_WaitFall \n\r");
      if (ThisEvent.EventType == FallingEdge){
        //printf("In EOC_WaitFall \n\r");
//				Set TimeOfLastFall to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
//				Set NextState to EOC_WaitRise
        NextState = EOC_WaitRise;
      }
      if (ThisEvent.EventType == DBButtonDown){
        //printf("In DBButtonDown \n\r");
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
      if (ThisEvent.EventType == EOCDetected){
         //printf("In EOCDetected \n\r");
//				Set NextState to DecodeWaitFall
         NextState = DecodeWaitFall;
      }
      //printf("In EOC_WaitFall END \n\r");
    }
    break;
    
    
    case DecodeWaitRise:{
      //printf("In DecodeWaitRise \n\r");
      if (ThisEvent.EventType == RisingEdge){
//				Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
//				Set NextState to DecodeWaitFall
        NextState = DecodeWaitFall;
//				Call CharacterizeSpace function
        CharacterizeSpace();
        //printf("Stuck in DecodeWaitRise \n\r");
      }
      if (ThisEvent.EventType == DBButtonDown){
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
    }
    break;
    
    
    case DecodeWaitFall:{
      //printf("In DecodeWaitFall \n\r");
      if (ThisEvent.EventType == FallingEdge){
//				Set TimeOfLastFall to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
//				Set NextState to DecodeWaitRise
        NextState = DecodeWaitRise;
//				Call CharacterizePulse function
        CharacterizePulse();
      }
      if (ThisEvent.EventType == DBButtonDown){
//				Set NextState to CalWaitForRise
        NextState = CalWaitForRise;
//				Set FirstDelta to 0
        FirstDelta = 0;
      }
    }
    break;
    
    
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  //printf("Through to end state \n\r");
  CurrentState = NextState;
  
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
State_t QueryMorseElements(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/****************************************************************************
 Function
     TestCalibration

 Parameters
     Takes no parameters, returns nothing.

 Returns
     none

 Description
     returns the current state of the Template state machine
 Notes
     Local variable SecondDelta
 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
void TestCalibration(void)
{
//  If calibration is just starting (FirstDelta is 0)
//		Set FirstDelta to most recent pulse width
//Else
//		Set SecondDelta to most recent pulse width
//		If (100.0 * FirstDelta / SecondDelta) less than or equal to 33.33
//			Save FirstDelta as LengthOfDot
//			PostEvent CalCompleted to MorseElementsSM
//		ElseIf (100.0 * FirstDelta / Second Delta) greater than 300.0
//			Save SecondDelta as LengthOfDot
//			PostEvent CalCompleted to MorseElementsSM
//		Else (prepare for next pulse)
//			SetFirstDelta to SecondDelta
//		EndIf

  
  if(FirstDelta == 0){
    FirstDelta = TimeOfLastFall - TimeOfLastRise;
  } else{
    SecondDelta = TimeOfLastFall - TimeOfLastRise;
    if( ((100.0*FirstDelta / SecondDelta) <= 35.0)  &&  ((100.0*FirstDelta / SecondDelta) >= 32.0)){
      LengthOfDot = FirstDelta;
      ES_Event_t ThisEvent;
      ThisEvent.EventType = CalibrationCompleted;
      PostMorseElements(ThisEvent);
    }else if( ((100.0*FirstDelta / SecondDelta) >= 280.0)  &&  ((100.0*FirstDelta / SecondDelta) <= 330.0)){
      LengthOfDot = SecondDelta;
      ES_Event_t ThisEvent;
      ThisEvent.EventType = CalibrationCompleted;
      PostMorseElements(ThisEvent);
    } else{
      FirstDelta = SecondDelta;
    }
  }
  printf(" Length of Dot = %u \n\r", LengthOfDot);

}

/****************************************************************************
 Function
     CharacterizeSpace

 Parameters
     Takes no parameters, returns nothing.

 Returns
     none

 Description
     Posts one of EOCDetected Event, EOWDetected Event, BadSpace Event as appropriate
      on good dot-space, does nothing
 Notes
     Local variable LastInterval, Event2Post
 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
void CharacterizeSpace(void)
{
//Calculate LastInterval as TimeOfLastRise � TimeOfLastFall
//If LastInterval not OK for a Dot Space
//	If LastInterval OK for a Character Space
//		PostEvent EOCDetected Event to Decode Morse Service & Morse Elements Service
//	Else
//		If LastInterval OK for Word Space
//			PostEvent EOWDetected Event to Decode Morse Service
//		Else
//			PostEvent BadSpace Event to Decode Morse Service
//		EndIf
//	Endif
//EndIf
  //printf("In CharSpace \n\r");
  uint16_t epsilon = LengthOfDot/3.0;
  uint16_t CharacterSpace = LengthOfDot*3;
  uint16_t WordSpace = LengthOfDot*7;
  uint16_t LastInterval = TimeOfLastRise - TimeOfLastFall;
  if(LastInterval > (LengthOfDot+epsilon)){
    if(LastInterval <= (CharacterSpace+epsilon*3) && (LastInterval >= (CharacterSpace-epsilon*3))){
      printf(" ");
      //printf(" EC%d ", LastInterval);
      ES_Event_t ThisEvent;
      ThisEvent.EventType = EOCDetected;
      PostMorseElements(ThisEvent);
      if((CurrentState == DecodeWaitRise) || (CurrentState == DecodeWaitFall)){
        PostDecodeMorse(ThisEvent);
      }
    }else{
      if(LastInterval <= (WordSpace+epsilon*7) && (LastInterval >= (WordSpace-epsilon*7))){
        ES_Event_t ThisEvent;
        ThisEvent.EventType = EOWDetected;
        //printf(" EW%d ", LastInterval);
        PostDecodeMorse(ThisEvent);
        printf("|");
      }else{
        ES_Event_t ThisEvent;
        ThisEvent.EventType = BadSpace;
        //printf(" B%d ", LastInterval);
        if((CurrentState == DecodeWaitRise) || (CurrentState == DecodeWaitFall)){
        PostDecodeMorse(ThisEvent);
        }
      }
    }
  }
  //printf("End CharSpace \n\r");
}


/****************************************************************************
 Function
     CharacterizePulse

 Parameters
     Takes no parameters, returns nothing.

 Returns
     none

 Description
     Posts one of DotDetectedEvent, DashDetectedEvent, BadPulseEvent
 Notes
     Local variable LastPulseWidth, Event2Post
 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
void CharacterizePulse(void)
{
//Calculate LastPulseWidth as TimeOfLastFall - TimeOfLastRise
//If LastPulseWidth OK for a dot
//		PostEvent DotDetected Event to Decode Morse Service
//Else
//		If LastPulseWidth OK for dash
//			PostEvent DashDetected Event to Decode Morse Service
//		Else
//			PostEvent BadPulse Event to Decode Morse Service
//		EndIf
//EndIf
  //printf("In CharPulse \n\r");
  
  uint16_t LastPulseWidth = TimeOfLastFall - TimeOfLastRise;
  if(LastPulseWidth <= (LengthOfDot+35) && (LastPulseWidth >= (LengthOfDot-32))){
    //printf(" DOT \n\r");
    ES_Event_t ThisEvent;
    ThisEvent.EventType = DotDetected;
    printf(".");
    //printf(" O%d ", LastPulseWidth);
    if((CurrentState == DecodeWaitRise) || (CurrentState == DecodeWaitFall)){
    PostDecodeMorse(ThisEvent);
    }
  }else {
    if(LastPulseWidth <= (LengthOfDot+3)*3 && LastPulseWidth >= (LengthOfDot-3)*3){
       //printf(" DASH \n\r");
       ES_Event_t ThisEvent;
       ThisEvent.EventType = DashDetected;
       printf("-");
       //printf(" D%d ", LastPulseWidth);
       PostDecodeMorse(ThisEvent);
    }else{
       ES_Event_t ThisEvent;
       ThisEvent.EventType = BadPulse;
       printf(" B%d ", LastPulseWidth);
      if((CurrentState == DecodeWaitRise) || (CurrentState == DecodeWaitFall)){
       PostDecodeMorse(ThisEvent);
      }
    }
  }
 
}




/****************************************************************************/
////TEST HARNESS
//#ifdef TEST
///*Test Harness for testing this module*/
//#include "termio.h"

//#include <stdint.h>
//#include <stdbool.h>
//#include <stdio.h>

//#include "inc/hw_types.h"
//#include "inc/hw_memmap.h"
//#include "driverlib/sysctl.h"

//#include "ES_Configure.h"
//#include "ES_Framework.h"
//#include "ES_Port.h"
//#include "termio.h"
//#include "EnablePA25_PB23_PD7_PF0.h"

//int main(void){
//	TERMIO_Init();
//  
//	// When doing testing, it is useful to announce just which program
//	// is running.
//	puts("\rStarting Basic Template \r");
//	printf("%s %s\n",__TIME__, __DATE__);
//	printf("\n\r\n");
//  
//  puts("\nHello\n");
//  
//  //initialize the LCD hardware
//  InitializeMorseElements(0);
//  
//  //Run the initialization (each step with a keystroke)
//  printf("\nstart\n");
//  while(true){
//    CheckMorseEvents();
//  }
//  
//  //sit at the end after all done
//  for(;;)
//	  ;
//}

//#endif
