/****************************************************************************
 Module
   ShiftRegisterWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to a write only shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass
 10/17/19 644   nat     lab3 usage
****************************************************************************/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

// readability defines
#define DATA GPIO_PIN_0

#define SCLK GPIO_PIN_1
#define SCLK_HI BIT1HI
#define SCLK_LO BIT1LO

#define RCLK GPIO_PIN_2
#define RCLK_LO BIT2LO
#define RCLK_HI BIT2HI

#define ALL_BITS (0xff<<2)

#define GET_MSB_IN_LSB(x) ((x & 0x80)>>7)

// an image of the last 8 bits written to the shift register
static uint8_t LocalRegisterImage = 0;

// Enables, sets as digital, sets direction, initializes values for Tiva Outputs
void SR_Init(void){

  // set up port B by enabling the peripheral clock, waiting for the 
  // peripheral to be ready and setting the direction
  // of PB0, PB1 & PB2 to output
	HWREG(SYSCTL_RCGCGPIO) |= (BIT1HI); //enable port b
	while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1){
		//wait for clcok to be ready
	}
	//set bits as digital, set data direction outputs
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DEN)) |= (BIT0HI | SCLK_HI | RCLK_HI); 
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DIR)) |= (BIT0HI | SCLK_HI | RCLK_HI);
	
  // start with the data & sclk lines low and the RCLK line high
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= SCLK_LO;
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;
}

// Gets the Current Register Value
uint8_t SR_GetCurrentRegister(void){
  return LocalRegisterImage;
}

// Writes the value from NewValue to have each of its bits on each of the 8 
// output ports on the shift register ('595) with bit7 on Q0 and bit0 on Q7
void SR_Write(uint8_t NewValue){
  uint8_t BitCounter;
  LocalRegisterImage = NewValue; // save a local copy

  // lower the register clock
	HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= RCLK_LO;
  
  // shift out the data while pulsing the serial clock 
	for (BitCounter = 0; BitCounter <= 7; BitCounter++){
		
    // Isolate the MSB of NewValue, put it into the LSB position and output to port
		if(GET_MSB_IN_LSB(NewValue) == 1){
			//sets the SDATA bit HI
			HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
		}else{
			//sets the SDATA bit LO
			HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
		}
    
    // raise SCLK
		HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= SCLK_HI;
		// lower SCLK
		HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= SCLK_LO;
    // finish looping through bits in NewValue
		NewValue = (NewValue<<1);
	}
	
  
// raise the register clock to latch the new data
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= RCLK_HI;
}
